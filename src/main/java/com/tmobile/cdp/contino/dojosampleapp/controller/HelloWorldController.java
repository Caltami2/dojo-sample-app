package com.tmobile.cdp.contino.dojosampleapp.controller;

import com.tmobile.cdp.contino.dojosampleapp.model.Greeting;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by ssajjad1 on 3/1/2017.
 */
@RestController
@Api(value="HelloWorld", description="Simple one-method api for returning 'hello-world'")
public class HelloWorldController {

    private static final String TEMPLATE = "Hello, %s!";

    private final AtomicLong counter = new AtomicLong();

    @ApiOperation(value = "Request a greeting", httpMethod = "GET")
    @GetMapping("/greeting")
    public
    Greeting sayHello(@RequestParam(value="name", required=false, defaultValue="Stranger") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(TEMPLATE, name));
    }
}
