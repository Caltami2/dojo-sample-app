package com.tmobile.cdp.contino.dojosampleapp.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name ="USER")
public class User {

	@Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

  @Column(name = "FIRST_NAME", nullable=false)
	private String name;

	@Column(name = "ROLE", nullable=false)
	private String role;

	protected User() {
	}

	public User(Long id, String name, String role) {
		this.id = id;
		this.name = name;
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", role=" + role + "]";
	}

}
