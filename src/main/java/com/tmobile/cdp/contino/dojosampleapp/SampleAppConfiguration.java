package com.tmobile.cdp.contino.dojosampleapp;

/**
 * Created by ssajjad1 on 3/1/2017.
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleAppConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(SampleAppConfiguration.class, args);
    }

}
