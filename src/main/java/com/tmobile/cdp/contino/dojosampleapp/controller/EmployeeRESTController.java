package com.tmobile.cdp.contino.dojosampleapp.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.validation.Valid;

import com.tmobile.cdp.contino.dojosampleapp.model.Employee;
import com.tmobile.cdp.contino.dojosampleapp.repository.EmployeeRepository;

@RestController
@Api(value="Employees", description="Operations pertaining to Employees")
@RequestMapping(value = "/v1/api/", produces = { MediaType.APPLICATION_JSON_VALUE })
public class EmployeeRESTController
{
    @Autowired
    private EmployeeRepository repository;

    public EmployeeRepository getRepository() {
        return repository;
    }

    public void setRepository(EmployeeRepository repository) {
        this.repository = repository;
    }

    @GetMapping(value = "/employees")
    @ApiOperation(value = "View a list of available employees", response = List.class, httpMethod = "GET")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved list of employees"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public List<Employee> getAllEmployees() {
        return repository.findAll();
    }

    @PostMapping("/employees")
    @ApiOperation(value = "Add an employee", httpMethod = "POST")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully added an employee. "),
        @ApiResponse(code = 400, message = "The request body was invalid. "),
        @ApiResponse(code = 409, message = "An employee with that ID already exists")
    })
    public Employee createOrSaveEmployee(
        @Valid
        @ApiParam(value = "Employee object store in database table", required = true)
        @RequestBody Employee newEmployee) {
        return repository.save(newEmployee);
    }

    @GetMapping("/employees/{id}")
    @ApiOperation(value = "Get an employee based on an employee id" , httpMethod = "GET")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved an employee"),
        @ApiResponse(code = 404, message = "The employee you were trying to retrieve is not found")
    })
    public Employee getEmployeeById(
        @ApiParam(value = "Employee id from which employee object will retrieve", required = true)
        @PathVariable Long id) {
        return repository.findById(id).get();
    }

    @PutMapping("/employees/{id}")
    @ApiOperation(value = "Update an employee", httpMethod = "PUT")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully updated an employee record"),
        @ApiResponse(code = 400, message = "The request could not be understood due to malformed syntax")
    })
    public Employee updateEmployee(@RequestBody Employee newEmployee, @PathVariable Long id) {

        return repository.findById(id).map(employee -> {
            employee.setFirstName(newEmployee.getFirstName());
            employee.setLastName(newEmployee.getLastName());
            employee.setEmail(newEmployee.getEmail());
            return repository.save(employee);
        }).orElseGet(() -> {
            newEmployee.setId(id);
            return repository.save(newEmployee);
        });
    }

    @DeleteMapping("/employees/{id}")
    @ApiOperation(value = "Delete an employee", httpMethod = "DELETE")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully deleted an employee record"),
        @ApiResponse(code = 404, message = "The employee you attempted to delete was not found")
    })
    public void deleteEmployee(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
