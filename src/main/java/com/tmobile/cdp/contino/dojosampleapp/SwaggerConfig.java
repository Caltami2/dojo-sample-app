package com.tmobile.cdp.contino.dojosampleapp;

import static springfox.documentation.builders.PathSelectors.regex;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    //TODO once package names are updated
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
            .apis(RequestHandlerSelectors
                .basePackage("com.tmobile.cdp.contino.dojosampleapp"))
            .paths(PathSelectors.regex("/.*"))
            .build().apiInfo(apiEndPointsInfo());
    }

    //TODO update contact information
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("EDP Hello World - REST API")
            .description("Automatically generated via Swagger.")
            .contact(new Contact("Chris Altamimi", "https://www.contino.io", "chris.altamimi@contino.io"))
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .version("1.0.0")
            .build();
    }
}
