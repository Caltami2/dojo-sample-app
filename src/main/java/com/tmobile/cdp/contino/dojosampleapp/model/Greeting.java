package com.tmobile.cdp.contino.dojosampleapp.model;

import io.swagger.annotations.ApiModel;
import javax.persistence.Entity;
/**
 * Created by ssajjad1 on 3/1/2017.
 */
 @ApiModel(value="Greeting", description="Greeting domain entity model.")
public class Greeting {

    private long id;
    private String greetingMessage;

    public Greeting(long id, String greetingMessage) {
        this.id = id;
        this.greetingMessage = greetingMessage;
    }

    public long getId() {
        return id;
    }

    public String getGreeting() {
        return greetingMessage;
    }
}
